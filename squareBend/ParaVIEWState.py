import os

# state file generated using paraview version 5.12.0
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 12

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1488, 729]
renderView1.AxesGrid = 'Grid Axes 3D Actor'
renderView1.CenterOfRotation = [-0.21249999850988388, 0.0, 0.0]
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [-0.21249999850988388, 0.0, 0.6503000807218775]
renderView1.CameraFocalPoint = [-0.21249999850988388, 0.0, 0.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 0.29817151326456065
renderView1.LegendGrid = 'Legend Grid Actor'

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(1488, 729)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'Open FOAM Reader'
dir = os.path.dirname(__file__)
squareBendfoam = OpenFOAMReader(registrationName='squareBend.foam', FileName=f'{dir}/squareBend.foam')
squareBendfoam.MeshRegions = ['internalMesh']
squareBendfoam.CellArrays = ['T', 'U', 'alphat', 'epsilon', 'isentropicP', 'k', 'nut', 'p', 'pTot', 'rho', 'wallHeatFlux']

# create a new 'Python Calculator'
pythonCalculator1 = PythonCalculator(registrationName='PythonCalculator1', Input=squareBendfoam)
pythonCalculator1.Expression = 'p / (rho * T)'
pythonCalculator1.ArrayAssociation = 'Cell Data'
pythonCalculator1.ArrayName = 'p/(rho*T)'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pythonCalculator1
pythonCalculator1Display = Show(pythonCalculator1, renderView1, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'prhoT'
prhoTTF2D = GetTransferFunction2D('prhoT')

# get color transfer function/color map for 'prhoT'
prhoTLUT = GetColorTransferFunction('prhoT')
prhoTLUT.TransferFunction2D = prhoTTF2D
prhoTLUT.RGBPoints = [287.6340637207031, 0.231373, 0.298039, 0.752941, 287.66648864746094, 0.865003, 0.865003, 0.865003, 287.69891357421875, 0.705882, 0.0156863, 0.14902]
prhoTLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'prhoT'
prhoTPWF = GetOpacityTransferFunction('prhoT')
prhoTPWF.Points = [287.6340637207031, 0.0, 0.5, 0.0, 287.69891357421875, 1.0, 0.5, 0.0]
prhoTPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
pythonCalculator1Display.Representation = 'Surface'
pythonCalculator1Display.ColorArrayName = ['CELLS', 'p/(rho*T)']
pythonCalculator1Display.LookupTable = prhoTLUT
pythonCalculator1Display.SelectTCoordArray = 'None'
pythonCalculator1Display.SelectNormalArray = 'None'
pythonCalculator1Display.SelectTangentArray = 'None'
pythonCalculator1Display.OSPRayScaleArray = 'p'
pythonCalculator1Display.OSPRayScaleFunction = 'Piecewise Function'
pythonCalculator1Display.Assembly = 'Hierarchy'
pythonCalculator1Display.SelectOrientationVectors = 'U'
pythonCalculator1Display.ScaleFactor = 0.05750000029802323
pythonCalculator1Display.SelectScaleArray = 'p'
pythonCalculator1Display.GlyphType = 'Arrow'
pythonCalculator1Display.GlyphTableIndexArray = 'p'
pythonCalculator1Display.GaussianRadius = 0.0028750000149011614
pythonCalculator1Display.SetScaleArray = ['POINTS', 'p']
pythonCalculator1Display.ScaleTransferFunction = 'Piecewise Function'
pythonCalculator1Display.OpacityArray = ['POINTS', 'p']
pythonCalculator1Display.OpacityTransferFunction = 'Piecewise Function'
pythonCalculator1Display.DataAxesGrid = 'Grid Axes Representation'
pythonCalculator1Display.PolarAxes = 'Polar Axes Representation'
pythonCalculator1Display.ScalarOpacityFunction = prhoTPWF
pythonCalculator1Display.ScalarOpacityUnitDistance = 0.012371531660044859
pythonCalculator1Display.OpacityArrayName = ['POINTS', 'p']
pythonCalculator1Display.SelectInputVectors = ['POINTS', 'U']
pythonCalculator1Display.WriteLog = ''

# init the 'Piecewise Function' selected for 'ScaleTransferFunction'
pythonCalculator1Display.ScaleTransferFunction.Points = [86772.703125, 0.0, 0.5, 0.0, 186061.0, 1.0, 0.5, 0.0]

# init the 'Piecewise Function' selected for 'OpacityTransferFunction'
pythonCalculator1Display.OpacityTransferFunction.Points = [86772.703125, 0.0, 0.5, 0.0, 186061.0, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for prhoTLUT in view renderView1
prhoTLUTColorBar = GetScalarBar(prhoTLUT, renderView1)
prhoTLUTColorBar.Title = 'p/(rho*T)'
prhoTLUTColorBar.ComponentTitle = ''

# set color bar visibility
prhoTLUTColorBar.Visibility = 1

# show color legend
pythonCalculator1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity maps used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup animation scene, tracks and keyframes
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get time animation track
timeAnimationCue1 = GetTimeTrack()

# initialize the animation scene

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# initialize the timekeeper

# initialize the animation track

# get animation scene
animationScene1 = GetAnimationScene()

# initialize the animation scene
animationScene1.ViewModules = renderView1
animationScene1.Cues = timeAnimationCue1
animationScene1.AnimationTime = 156.0
animationScene1.StartTime = 100.0
animationScene1.EndTime = 156.0
animationScene1.PlayMode = 'Snap To TimeSteps'

# ----------------------------------------------------------------
# restore active source
SetActiveSource(pythonCalculator1)
# ----------------------------------------------------------------


##--------------------------------------------
## You may need to add some code at the end of this python script depending on your usage, eg:
#
## Render all views to see them appears
# RenderAllViews()
#
## Interact with the view, usefull when running from pvpython
# Interact()
#
## Save a screenshot of the active view
# SaveScreenshot("path/to/screenshot.png")
#
## Save a screenshot of a layout (multiple splitted view)
# SaveScreenshot("path/to/screenshot.png", GetLayout())
#
## Save all "Extractors" from the pipeline browser
# SaveExtracts()
#
## Save a animation of the current active view
# SaveAnimation()
#
## Please refer to the documentation of paraview.simple
## https://kitware.github.io/paraview-docs/latest/python/paraview.simple.html
##--------------------------------------------